#!/usr/bin/env bash

# Set up repository
apt-get -y update
apt-get -y upgrade

# Make folders and set permissions
mkdir -p /var/log/apache/

# Install mysql
debconf-set-selections <<< "mysql-server mysql-server/root_password password vagrant"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password vagrant"
apt-get install -y mysql-server

# Install apache
apt-get install -y apache2
a2enmod rewrite

# Install php
apt-get install -y php libapache2-mod-php php-curl php-mcrypt php-mysql php-xml php-mbstring php-zip

# Install git
apt-get install -y git

# Install Composer
apt-get install -y composer

# Copy vHosts
yes | cp -rf /vagrant/devops/vhosts/*.conf /etc/apache2/sites-available/

# Rewrite hosts file
yes | cp -rf /vagrant/devops/configs/hosts /etc/hosts

# Rewrite php.ini
yes | cp -rf /vagrant/devops/configs/php.ini /etc/php.ini

# Rewrite my.cnf
yes | cp -rf /vagrant/devops/configs/my.cnf /etc/my.cnf

# Remove default index.html
rm -rf /var/www/html/index.html

# Install latest composer
sh /vagrant/devops/configs/composer.sh

# Restart services
service apache2 restart
service mysql restart
