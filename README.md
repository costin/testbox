Simple LAMP box based on Ubuntu 16.04 LTS (Xenial Xerus)

Included PHP 7, MySQL 5, Apache 2

####Prerequisites:
- Vagrant 2+
- Vagrant plugins:
 - ```vagrant-hostsupdater```
 - ```vagrant-vbguest```

####How to start/run:
- clone the repo
- change directory to where the Vagrant file is located
- run ```vagrant up```

Once the vm has started, you can place your application in the ```projectfiles``` folder.
The application is accessible in the host machine at ```http://testbox.local```